<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class booksborrowed extends Model
{
    protected $fillable = [
    	'memberid', 'bookid',
	];

	public function book(){
    	return $this->belongsTo('App\book');
	}
}
