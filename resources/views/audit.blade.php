<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Home</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{url('vendor/bootstrap-4.1.3/css/bootstrap.min.css')}}" >
    </head>
    <body>
        <!-- As a link -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="{{ url('/')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/booksborrowed')}}">Books Issued</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Register Member</a>
            </li>
            
          </ul>
        </div>
    </nav>
    <div class="container">
       <div class="row">
           <div class="col col-md-12">
               <h4>Audit Report</h4>
               <table class="table table-bordered ">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Action</th>
            <th>Description</th>
            <th>Table Affected</th>
            <th>Record ID</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1.</td>
            <td>Kenneth Sigei</td>
            <td>Updated</td>
            <td>Updated book number</td>
            <td>Books table</td>
            <td>1</td>
            <td>2019-06-24 16:00:20</td>
          </tr>
        </tbody>
      </table>
               
           </div>
           <div class="col col-md-4"></div>
           <div class="col col-md-4"></div>
       </div> 
    </div>
    </body>
</html>
