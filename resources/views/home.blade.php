<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Home</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{url('vendor/bootstrap-4.1.3/css/bootstrap.min.css')}}" >
    </head>
    <body>
        <!-- As a link -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="{{ url('/')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/booksborrowed')}}">Books Issued</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Register Member</a>
            </li>
            
          </ul>
        </div>
    </nav>
    <div class="container">
       <div class="row">
           <div class="col col-md-4">
               <h4>Issue Book</h4>
               <form method="POST" action="{{ url('/')}}">
                        @csrf
                        <div class="form-group">
                            <label for="memberid" >ID Number</label>
                            <input id="memberid" class="form-control" type="text" name="memberid" required autofocus placeholder="ID Number">
                        </div>

                        <div class="form-group">
                            <label for="bookid" >Book Id</label>
                            <input id="bookid" class="form-control" type="text" name="bookid"required autofocus placeholder="Book Id">
                        </div>
                        
                        <button type="submit" class="btn btn-primary btn-sm btn-block">
                            Issue
                        </button>

                    </form>
           </div>
           <div class="col col-md-4"></div>
           <div class="col col-md-4"></div>
       </div> 
    </div>
    </body>
</html>
